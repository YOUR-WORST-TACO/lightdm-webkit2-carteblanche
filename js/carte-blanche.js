var _greeter_carte = null;

function getCookie(cookie) {
	var cookies = decodeURIComponent(document.cookie)
}

function imageExists(url) {
	var img = new Image();
	img.src = url;
	return img.height != 0;
}

class CarteGreeter {
	// make sure we do not create more than one instance of this class
	constructor() {
		if ( null !== _greeter_carte ) {
			return _greeter_carte; // return _greeter if it already exists
		}

		this.user = null; // init user variable
		this.session = null; // init session variable
		this.a_color = null; // accent color

		this.tog_user = false // keep track of drop down
		this.cache_backend = null; // to store our storage shits

		this.$background = $('#background'); // get our background div
		this.$blurContainer = $('#blur-container'); // get blur container div
		this.$blur = $('#blur'); // get blur div 

		this.$user = $('#user'); // get user h1
		this.$users = $('#users'); // get users list
		this.$sessions = $('#sessions'); // get sessions list
		this.$userlen = 0 // store how many users we have
		this.$pass = $('#password-input'); // get password input (NOT WORKING!)
		this.$delim = $('#delim'); // get @ symbol
		this.$host = $('#host'); // get host h1

		_greeter_carte = this; // give _greeter_carte an instance

		return _greeter_carte; // return object
	}

	initialize() {

		this.setupNetStorage()
		this.buildSessions();
		this.getCacheUser();
		this.getCacheSession();

		this.$user.text(this.user.username); // update username text 
		this.$host.text(lightdm.hostname); // update hostname text 
		this.userlen = lightdm.users.length;

		var pos = this.$blurContainer.offset(); // get blur container position

		var wallpaper = _conf["background_image"];

		

		this.$background.css({"background-image": "url(" + wallpaper +")"}); // update urls for background
		
		if (_conf["background_blur_image"] != "") {
			wallpaper = _conf["background_blur_image"];
		}

		this.$blur.css({"top": -pos.top, "left": -pos.left, "background": "linear-gradient(rgba(255,255,255,0), rgba(255,255,255,0.7)), url(" + wallpaper + ")", "background-size": "cover"});

		this.$delim.css({"color": _conf["accent_color"]});

		$(document).on( 'keydown', event => this.key_press_handler(event) ); // connect keydown handle
		
		this.$user.hover(function(){
				if (carte.greeter.userlen > 1) {
					$("#user").css("color", _conf["accent_color"]);
				}
			}, function(){
				$("#user").css("color", "black");
			}
		)

		this.$user.on("click", function() {
			if (carte.greeter.userlen > 1) {
				$("#users li").slideToggle(100);
			}
		})

		// create global functions that point to the class methods 
		window.show_prompt = this.show_prompt;
		window.show_message = this.show_message;
		window.start_authentication = this.start_authentication;
		window.cancel_authentication = this.cancel_authentication;
		window.authentication_complete = this.authentication_complete;
		window.autologin_timer_expired = this.cancel_authentication;

		$("#password").focus() // focus password input

		this.start_authentication(this.user.username); // begin authentication 
	}

	// originally from antergos greeter, 
	// I claim no ownership of this method
	setupNetStorage() {
		try {
			localStorage.setItem( 'testing', 'test' );

			if ( 'test' === localStorage.getItem( 'testing' ) ) {
				// We have access to localStorage
				this.cache_backend = 'localStorage';
			}

			localStorage.removeItem( 'testing' );

		} catch ( err ) {
			// We do not have access to localStorage. Use cookies instead.
			log( err );
			log( 'INFO: localStorage is not available. Using cookies for cache backend.' );
			this.cache_backend = 'Cookies';
		}
	}

	getCacheUser() {
		if (this.cache_backend == 'localStorage') { // we are using localstore
			var storedUser = localStorage.getItem("login_user"); // grab stored var

			if (storedUser != null) { // if store is not null
				if (!this.selectUser(storedUser)) { // check if we can find it
					this.selectUser(lightdm.users[0].username); // otherwise use default
				}
			} else {
				this.selectUser(lightdm.users[0].username);
			}
		} else { // otherwise we are using Cookies
			console.log("using cookies")
		}
	}

	selectUser( user ) {
		var i;
		for (i = 0; i < lightdm.users.length; i++) {
			if (lightdm.users[i].username == user) {
				this.user = lightdm.users[i];
				this.$user.text(this.user.username); // update username text
				$("#users li").slideToggle(100);
				this.updateUsers();
				this.start_authentication(this.user.username);

				return true;
			}
		}
		return false;
	}

	updateUsers() {
		this.$users.text("");

		var i;
		for (i = 0; i < lightdm.users.length; i++) {
			if (lightdm.users[i].username != this.user.username) {
				var u = lightdm.users[i];
				var id = "click_" + u.username
				this.$users.append('<li><a onclick=\'carte.greeter.selectUser("' + u.username + '")\' href="#" id=' + id + '>' + lightdm.users[i].username + '</a></li>');
			
			}
		}
	}

	getCacheSession() {
		if (this.cache_backend == 'localStorage') { // we are using localstore
			var storedSession = localStorage.getItem("login_session");

			if (storedSession != null) {
				if (!this.selectSession(storedSession)) {
					this.selectSession(lightdm.sessions[0].key);
				}
			} else {
				this.selectSession(lightdm.sessions[0].key);
			}
		} else { // otherwise we are using Cookies
			console.log("problems")
		}
	}

	selectSession( session ){
		var i;
		for (i = 0; i < lightdm.sessions.length; i++) {
			if (lightdm.sessions[i].key == session) {
				this.session = lightdm.sessions[i];
				$('.sessionimg').css('filter', 'grayscale(100%)');
				$('#' + session + '_img').css('filter', 'grayscale(0%)');
				return true;
			}
		}
		return false;
	}

	buildSessions() {
		var i;
		for (i = 0; i < lightdm.sessions.length; i++) {
			var key = lightdm.sessions[i].key;
			var image = "icons/" + key + ".png";
			this.$sessions.append('<li><a onclick=\'carte.greeter.selectSession("' + key + '")\' href="#"><img title="' + key + '" id="' + key + '_img" class="sessionimg" src="' + image + '" onerror="this.src = \'icons/error.png\'"></a></li>')
		}
	}

	key_press_handler(event) {

		if (!$('#password').is(":focus")) { // check if password is out of focus
			$('#password').focus(); // if so focus it
		}

		var action = null // default action to null
		switch ( event.which ) { // switch to catch button press events
			case 13:
				action = this.submit_password;
		}

		if (null !== action) { // check if our action is null
			action();
		}
	}

	show_message( text, type ) { // this does nothing at the moment
		console.log(text)
	}

	show_prompt(text, type) { // this does nothing at the moment
		//lightdm.respond($('#password').val());
	}

	submit_password( event ) { // called to shoot lightdm a password
		var pass = $('#password').val(); // get the password from input 
		lightdm.respond(pass); // send password to lightdm 
		$('.hideme').animate({
			opacity: 0,
		}, 400, () => {
			$('.hideme').css('visibility', 'hidden');
			$('#thinker').fadeIn(400);
		});

		if (carte.greeter.cache_backend == 'localStorage') { // we are using localstore
			localStorage.setItem("login_session", carte.greeter.session.key);
			localStorage.setItem("login_user", carte.greeter.user.username);
		} else { // otherwise we are using Cookies
			console.log("trying cookie")
		}

		// REMOVE ME WHEN DONE WITH DEBUG
		//localStorage.setItem("login_session", carte.greeter.session.key);
		//localStorage.setItem("login_user", carte.greeter.user.username);
	}

	start_authentication(user) {
		if (!lightdm.in_authentication) {
			//lightdm.cancel_authentication();
			lightdm.authenticate(user); // start authentication with lightdm 
		}
	}

	cancel_authentication( event ) {
		lightdm.cancel_authentication(); // stop authentication with lightdm 
	}

	authentication_complete() {
		if ( !lightdm.is_authenticated ) { // if we failed to authenticate
			carte.greeter.start_authentication(carte.greeter.user.username); // restart authentication
			$("#password").val(''); // reset password value 

			$('#thinker').fadeOut(0);

			$('.exx').fadeIn(100);

			$('.exx').fadeOut( 700, () => {
				$('.hideme').css('visibility', 'visible');
				$('.hideme').animate({
					opacity: 1,
				}, 200);
			})
		} else { // otherwise do a little animation and send us in
			$( 'body' ).fadeOut( 500, () => {
				lightdm.login(lightdm.authenticated_user, carte.greeter.session.key);
			});
		}
	}
}

var carte = {}; // global instance of our Greeter
carte.greeter = new CarteGreeter(); // create the greeter

$( window ).on( 'load', () => {
	carte.greeter.initialize(); // once window is loaded we call initialize() 
});
